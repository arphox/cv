## 2024-06-28

### ✅ Graduate Developer
_(I checked everything in this category)_



### ✅ Developer
_(I checked everything in this category)_



### ✅ Senior Developer
_(I checked everything in this category)_



### 📈 Senior Engineer

<details><summary>Details</summary>

✅ I am a consultant, an architect, and a communicator and a problem solver. I am the go-to person in the general technology area. I represent the technical brilliance that our company brings to the market.

#### ✅ I am known for my technical knowledge

#### ✅ I am adaptable

#### 📈 I am emerging as a thought leader in technology and architecture
- 📈 I have a wide understanding of software development technology and practices, backed by extensive practical experience.
- 📈 I am confident making architectural decisions taking concerns like infrastructure, identity management, security, scalability, performance, concurrency and maintainability into consideration.
- ✅ I am comfortable with transparently assessing risk, making recommendations, escalating appropriately and dealing with the consequences along the way.
- ✅ I can apply my technical abilities to productively solve business problems with confidence and pragmatism.
- ✅ I am comfortable with building a product vision with a customer based on their business needs, regardless of whether this involves technology or otherwise.
- ✅ I use my skills, speed and knowledge to help my team be very productive.

#### ✅ I communicate technical concepts confidently and clearly

#### ✅ I am always learning, always teaching, always collaborating

</details>



### 📈 Senior Consultant

<details><summary>Details</summary>

📈 I am a well-rounded developer, architect, problem solver and leader of people. I represent the value our company brings to the market.

#### I am trusted to lead a small team (of up to 3) to ship a full product from idea to production with limited supervision.
- 📈 I am emerging as a leader of people.
- 📈 I can confidently step outside my comfort zone and adapt quickly to new team situations.
- 📈 I am comfortable diving head-first into moderately risky environments with relatively little upfront information and oversight.
- 📈 I am confident making architectural decisions taking concerns like infrastructure, identity management, security, scalability, concurrency and maintainability into consideration.
- ✅ I am comfortable with transparently assessing risk, making recommendations, escalating appropriately and dealing with the consequences along the way.
- ✅ I am comfortable with building a product vision with a customer based on their business needs, regardless of whether this involves technology or otherwise.
- 📈 I am proficient at leading software projects using agile practices.
- ✅ I can confidently and charismatically pitch ideas and influence my team and my customer's decisions.
- 📈 I am an adept communicator, and can effectively steer technical and non-technical conversations to positive outcomes over any medium.

#### ✅ I help our team focus on delivering value to our customers.

#### I am investing back into our company.
- ✅ I am actively involved in the retrospective process and am comfortable challenging and coaching my team.
- 📈 I leave my clients with a clear sense of why they have invested in our company.
- 📈 I proactively call out opportunities for our company with my clients.
- ✅ I am mentoring another company member, deliberately helping them to round out their skill set.
- ✅ I am sharing my on-the-job learning and experiences with others so they can be more effective in their roles.
- 📈 I am known as someone who looks for opportunities to invest into the our company teams in my state.

</details>



### 📈 Technical Lead

<details><summary>Details</summary>

✅ I am a well-rounded developer, problem solver and mentor for people. I represent the unconventional value our company brings to the market.

#### 📈 I focus on delivering value to our customers
- 📈 I can confidently step outside my comfort zone and adapt quickly to new team situations.
- 📈 I am confident diving head-first into moderately risky environments with relatively little upfront information and oversight.
- 📈 I am confident making architectural decisions around infrastructure, identity, security, scalability and maintainability.
- ✅ I am confident with transparently assessing risk, making recommendations, escalating appropriately and dealing with the consequences along the way.
- 📈 I am proficient at following agile methodologies.
- ✅ I can confidently and charismatically pitch ideas and influence my team and my customer's decisions.

#### 📈 I am trusted to on-board and deliver sustained engineering services to our customers with limited supervision.
- (N/A) I can on-board new customers into Managed Services.
- ✅ I can confidently talk to customers about our company services and solutions.
- 📈 I establish processes and leverage frameworks that promote visibility and accountability, while enabling effective expectation management and autonomous operation.
- (N/A) I am comfortable with building a roadmap for customer's environment to meet their needs.
- (N/A) I understand the business domain for my customer and their systems.
- 📈 I know how to stand-up and maintain a build and delivery pipeline.

#### 📈 I am investing back into our company.
- ✅ I am actively involved in the retrospective process and am comfortable challenging and coaching my team.
- 📈 I leave my clients with a clear sense of why they have invested in our company.
- 📈 I proactively call out opportunities for our company with my clients.
- ✅ I am mentoring other developers, deliberately helping them to round out their skill set.
- ✅ I am collaborating with other Technical Leads and Consultants in the fields so we can learn from each other's experiences.

 
#### ✅ I am a technical mentor.

</details>



### 🌱 Lead Engineer – still out of my reach in general



### 🌱 Lead Consultant – still out of my reach in general



### 🌱 Principal Consultant – still out of my reach in general



## Resources

```
                  Graduate Developer
                        |
                    Developer
                        |
                  Senior Developer
              /         |            \ 
Senior Engineer   Senior Consultant    Technical Lead               ⬅️ This is the line I'm around. More focused into engineering.
        |               |                     |
 Lead Engineer     Lead Consultant    Managed Services Team Lead
                        |
                Principal Consultant
```

- Repository: [Readify/madskillz: Readify Mad Skillz](https://github.com/Readify/madskillz/tree/master)
- Development & Consultant: [madskillz/Development.md](https://github.com/Readify/madskillz/blob/master/Development.md)
- Engineering: [madskillz/Engineering.md](https://github.com/Readify/madskillz/blob/master/Engineering.md)
- Managed Services: [madskillz/Managed Services.md](https://github.com/Readify/madskillz/blob/master/Managed%20Services.md)