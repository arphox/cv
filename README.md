# Károly Ozsvárt
#### A quality-oriented software engineer
**Links:** [LinkedIn](https://www.linkedin.com/in/arphox/), [GitHub](https://github.com/arphox), [GitLab](https://gitlab.com/arphox), [YouTube](https://www.youtube.com/@ozsvartkaroly), [Stackoverflow](https://stackoverflow.com/users/4215389), [Facebook](https://www.facebook.com/ozsvart.karoly/)

------------------------

**I am an efficiency and quality oriented, precise and proactive software engineer and C# developer**, working in the software industry **since 2017**.

### 1. Job titles I'm interested at:
- Staff Engineer (with backend .NET focus)
- C# .NET Software Engineer / Backend Developer

### 2. Hard skills
I write clean code and apply my deep knowledge in C# .NET development.  
I have experience developing **Web API**s in .NET.

I know:
- **developer tools**: Rider, Notepad++, Visual Studio, ReSharper, dotTrace, dotMemory, Postman, BenchmarkDotNet, NCrunch etc.
- **test frameworks**: xUnit, NUnit, MsTest (+ Moq/NSubstitute, FluentAssertions)
- **source control**: Git (via [Fork](https://fork.dev/)), TFS
- **persistence and platforms**: CosmosDb, MSSQL (+EF), some Kafka and Redis
- **devops basics**: CI/CD pipelines, Azure, docker, kubernetes
- **principles**: SOLID, DRY, KISS, YAGNI, etc.
- **design patterns**: DI, factory, builder, adapter, template method, strategy, etc
- **Azure**
- **AWS**

I have minor experience with: WCF, WPF, WinForms, Hangfire, and event sourcing.

### 3. Personal traits

**I am**:
- analytic
- system-thinking
- honest and outspoken (candid)
- concise
- competitive

**I love**:
- code quality/maintainability/performance
- feedback
- improving
- sharing my knowledge

**I don't like** monotonous work that does not require creativity and/or can be automated. I love automation.

### 4. Experience

#### 2024.08 – _present_: Staff Engineer (.NET) at [Nexius Learning](https://nexiuslearning.com/)
TBD

#### 2023.05 – 2024.08: Team Architect and C# Backend Developer at [Nexius Learning](https://nexiuslearning.com/)
Same module as previously mentioned, but in a different role with different challenges – similar to a technical team lead.  
Responsible for the technical aspect of the module, assisted the product owner in issue priorization and estimation, coordinated tasks, managed technical debts, coordinated with other teams on technical issues related to the owned module.

#### 2022.01 – 2023.05: Senior C# Software Developer at [Nexius Learning](https://nexiuslearning.com/)
Worked on the backend of a module of the Nexius E-learning system. The module is reponsible to serve learning content to students and test their knowledge through automated test assessment.  
Developed HTTP APIs using .NET 6/C# 9.  
Experienced an Azure cloud-first architecture. I learned the basics of Azure, deepened into CosmosDb, worked with App Services, Azure Functions and Blob Storage.  
_Note: worked there through [VividMind Zrt.](https://vividmindsoft.com) until 2024.01_

#### 2021.04 – 2022.01: C# Software Engineer at [Betsson Group](https://www.betssongroup.com/)
Worked on the backend software system which served betsson.com and other similar brands.
I developed HTTP APIs using C# .NET (mostly framework 4.7.x and Core 3.1) to implement features.
Met some new technologies & tools, notably: Couchbase, Consul, TeamCity, Octopus.
I experienced what it is like to work in a multinational cross-located team, and improved my English as well as my communication skills.

#### 2019.07 – 2021.04: Experienced C# Software Developer at [Grape Solutions](https://grape.hu/hu/)  
2020.07 – 2021.04: Worked on an internal product of the company. I developed in a microservices architecture, learned Kafka, worked with ASP.NET Core 3.1 Web API, C# 8, MongoDB, Elasticsearch and Redis. I also wrote, maintained and was responsible for an internal NuGet package.

2019.07 – 2020.07: Worked on the core backend systems of WizzAir.com, developing ASP.NET Web APIs in C# (.NET framework), using SQL and Entity Framework. I developed in an architecture similar to service oriented architectures.

#### 2018.04 – 2019.07: C# Software Engineer at [evosoft](https://www.evosoft.hu/)
Worked on a huge desktop application's internal component (GUI development not included). The application supported industrial automation, and was one of the largest C# code bases in the world.

#### 2017.07 – 2018.04: Junior C# Backend Developer at [DGITAL](https://www.dgital.com/)
Worked on a backend of a (flight industry) website in ASP.NET Web API, C#, using Oracle/Microsoft SQL, Entity Framework, Elasticsearch, Redis and Hangfire. I also met Swagger and experienced CI/CD pipelines.

#### Until 2017
I was working for 1.5 years as a programming teacher and graduated as a **Computer Science Engineer Bsc. at Óbuda University**,
specializing at enterprise information systems.

### 5. Other
Some facts about me:
- I am proficient in English language (I have certification for B2 level, but am around B2-C1 level both orally and in writing)
- I attended several (15+) professional competitions so far (and [won](https://itmap.hu/harom-versenyzo-harom-gyoztes-harom-ujraindulo/) some)
- I attended some courses related to computer networks (CISCO CCNA, MTA networking fundamentals)
- I have created [several video tutorials](https://www.youtube.com/c/ozkari93/videos) on C# programming on YouTube
- Most of my personal code is open source and public, [see them all here](https://gitlab.com/users/arphox/groups)
- I regularly read professional literature and watch conference presentations to extend my knowledge
- I love: video games 🎮, playing the piano 🎹, learning about various stuff 📖

---

If you are curious, see more details in [Details.md](./Details.md)