# More details

## Achievements

- **2024-06-20**: I gave a talk on C#-based Git hooks on [BeerUP Tech Fest 2024](https://www.linkedin.com/posts/hellomndwrk_beeruptechfest-beerup-itfest-activity-7203408126421659650-KRgK/) ([promo link](https://www.linkedin.com/feed/update/urn:li:activity:7206305220690104321/)). Recording: [BeerUP Tech Fest 2024: Ozsvárt Károly - Git hookok C# alapon - YouTube](https://www.youtube.com/watch?v=wfpaFQ7iZCs)